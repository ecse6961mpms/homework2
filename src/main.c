#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include <zlib.h>
#include <pthread.h>

#include "tqueue.h"

#define BLOCK_SIZE 4096

typedef struct thread_args_t{
    uint8_t* indata;
    uint8_t* outdata;
    size_t comp_size;
} thread_args_t;

void* writer(void* args);
void* worker(void* args);

tqueue* busy_queue;
tqueue* avail_queue;
pthread_mutex_t busy_queue_mutex;
pthread_mutex_t avail_queue_mutex;

pthread_t* threads;
thread_args_t* thread_args;
int done_read;

int main(int argc, char** argv){
    if(argc < 3){
        printf("Usage: %s infile outfile [workers]\n",argv[0]);
        return 1;
    }

    printf("infile: %s\n", argv[1]);

    int num_threads = 1;
    if(argc > 3) num_threads = atoi(argv[3]);
    printf("%d worker threads\n", num_threads);

    busy_queue =  tqueue_new();
    avail_queue = tqueue_new();
    pthread_mutex_init(&busy_queue_mutex, NULL);
    pthread_mutex_init(&avail_queue_mutex, NULL);
    threads = malloc(num_threads * sizeof(pthread_t));
    thread_args = malloc(num_threads * sizeof(thread_args_t));

	//Allocate memory for each worker's block
    for(int i = 0; i < num_threads; i++){
        thread_args[i].indata = malloc(BLOCK_SIZE);
        thread_args[i].outdata = malloc(BLOCK_SIZE);
        if(!thread_args[i].indata || !thread_args[i].outdata){
            printf("worked mem alloc failed for worker %d\n", i);
            return 1;
        }
        tqueue_e* e = tqueue_e_new(i);
        tqueue_add(avail_queue, e);
    }
	
	//Open files
    FILE* infile = fopen(argv[1], "rb");
    if(!infile){
        printf("could not open \"%s\"\n", argv[1]);
        return 1;
    }

    FILE* outfile = fopen(argv[2], "wb");
    if(!outfile){
		fclose(infile);
        printf("could not open \"%s\"\n", argv[2]);
        return 1;
    }

    pthread_t writer_thread;
    pthread_create(&writer_thread, NULL, writer, outfile);

    size_t read;
    done_read = 0;
    size_t total_read = 0;
    while(!done_read || !tqueue_isempty(busy_queue)){

        while(!done_read && !tqueue_isempty(avail_queue)){
            pthread_mutex_lock(&avail_queue_mutex);
            tqueue_e* e = tqueue_pop(avail_queue);
            pthread_mutex_unlock(&avail_queue_mutex);
            read = fread(thread_args[e->tnum].indata, 1, BLOCK_SIZE, infile);
            // printf("read %lu byes\n", read);
            total_read += read;
            if(read){
                // printf("creating worker %d\n", e->tnum);
                if(read < BLOCK_SIZE) memset(thread_args[e->tnum].indata+read, 0, BLOCK_SIZE-read);
                pthread_create(&threads[e->tnum], NULL, worker, &thread_args[e->tnum]);
                pthread_mutex_lock(&busy_queue_mutex);
                tqueue_add(busy_queue, e);
                pthread_mutex_unlock(&busy_queue_mutex);
            } else done_read = 1;
        }
    }

    pthread_join(writer_thread, NULL);

    printf("done\n");
    printf("read %lu bytes\n", total_read);

    fclose(outfile);
    fclose(infile);

    for(int i = 0; i < num_threads; i++){
        free(thread_args[i].indata);
        free(thread_args[i].outdata);
    }
    tqueue_delete(busy_queue);
    tqueue_delete(avail_queue);

    return 0;
}

void* writer(void* args){
    FILE* outfile = (FILE*)args;
    size_t total_wrote = 0;
    while(!done_read || !tqueue_isempty(busy_queue)){
        if(!tqueue_isempty(busy_queue)){
			//Use a mutex to get the latest element
            pthread_mutex_lock(&busy_queue_mutex);
            tqueue_e* e = tqueue_pop(busy_queue);
            pthread_mutex_unlock(&busy_queue_mutex);
            // printf("waiting for worker %d\n", e->tnum);
            pthread_join(threads[e->tnum], NULL);
            // printf("writing %ld compressed bytes\n", thread_args[e->tnum].comp_size);
            total_wrote += fwrite(thread_args[e->tnum].outdata, 1, thread_args[e->tnum].comp_size, outfile);
            pthread_mutex_lock(&avail_queue_mutex);
            tqueue_add(avail_queue, e);
            pthread_mutex_unlock(&avail_queue_mutex);
        }
    }
    printf("wrote %lu bytes\n", total_wrote);
    return 0;
}

void* worker(void* args){
    // printf("Inside thread\n");
    thread_args_t* thread_args = (thread_args_t*)args;
    int ret;
    z_stream strm;
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, -1);
    if(ret != Z_OK) printf("deflateInit error\n");
    strm.avail_in = BLOCK_SIZE;
    strm.next_in = thread_args->indata;
    strm.avail_out = BLOCK_SIZE;
    strm.next_out = thread_args->outdata;
    ret = deflate(&strm, Z_FINISH);
    // printf("ret = %d, avail_in = %d, avail_out = %d \n", ret, strm.avail_in, strm.avail_out);
    assert(strm.avail_in == 0);
    thread_args->comp_size = 4096 - strm.avail_out;
    deflateEnd(&strm);

    return 0;
}

