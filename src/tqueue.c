#include "tqueue.h"

#include <stdlib.h>


tqueue* tqueue_new(){
    tqueue* queue = malloc(sizeof(tqueue));
    if(!queue) return NULL;
    queue->head = NULL;
    queue->tail = NULL;
    return queue;
}


tqueue_e* tqueue_e_new(int tnum){
    tqueue_e* entry = malloc(sizeof(tqueue_e));
    if(!entry) return NULL;
    entry->tnum = tnum;
    entry->next = NULL;
    entry->prev = NULL;
    return entry;
}

void tqueue_add(tqueue* queue, tqueue_e* entry){
	/* 
	 * Places element at the head of the tqueue
	 * If the tqueue is empty, the element will also be the tail
	 */
    if(queue->tail == NULL){
        queue->head = entry;
        queue->tail = entry;
        return;
    }

    queue->tail->prev = entry;
    entry->next = queue->tail->prev;
    entry->prev = NULL;

    queue->tail = entry;
    return;
}

tqueue_e* tqueue_pop(tqueue* queue){
	/*
	 * Pop the first element from the queue's linked list.
	 * Get the head, then remove the references to it
	 * then set the next element as the new head
	 */
	
    if(queue->head == NULL) return NULL;

    tqueue_e* entry = queue->head; 
    if(entry->prev != NULL)entry->prev->next = NULL;
    queue->head = entry->prev;
    if(queue->tail == entry) queue->tail = NULL;
    entry->next = NULL;
    entry->prev = NULL;
    return entry;
}

int tqueue_isempty(tqueue* queue){
    return (queue->head == NULL);
}

void tqueue_delete(tqueue* queue){
	//Iterate over the linked list and free each entry
	tqueue_e* curr = NULL;
    tqueue_e* next = queue->tail;
    while(next){
        curr = next;
		next = curr->prev;
		free(curr);
    }
	free(queue);
}

