CC := gcc
CFLAGS := -march=native -O4

TARGET:= comp

BUILD_DIR := ./build
SRC_DIR := ./src
INC_DIR := ./inc

SRCS := $(shell find $(SRC_DIRS) -name *.c)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
INC_FLAGS := $(addprefix -I,$(INC_DIR)) -lz -lpthread

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(INC_FLAGS) -o $@

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC_FLAGS) -c $< -o $@

clean:
	rm -r $(BUILD_DIR)
	rm $(TARGET)
