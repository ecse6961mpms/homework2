#ifndef __TQUEUE_H__
#define __TQUEUE_H__

typedef struct tqueue tqueue;
typedef struct tqueue_e tqueue_e;

struct tqueue{
    tqueue_e* head;
    tqueue_e* tail;
};

struct tqueue_e{
    int tnum; 
    tqueue_e* next;
    tqueue_e* prev;
};

tqueue* tqueue_new();
tqueue_e* tqueue_e_new(int tnum);
void tqueue_add(tqueue* queue, tqueue_e* entry);
tqueue_e* tqueue_pop(tqueue* queue);
int tqueue_isempty(tqueue* queue);
void tqueue_delete(tqueue* queue);

#endif
